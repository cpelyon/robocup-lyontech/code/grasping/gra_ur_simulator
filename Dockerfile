FROM universalrobots/ursim_e-series

# Install the URCap
COPY ressources/externalcontrol-1.0.5.urcap /urcaps/externalcontrol-1.0.5.jar

COPY ressources/programs.UR5 /ursim/programs.UR5

# Install pre-made programs
#COPY programs ursim/programs