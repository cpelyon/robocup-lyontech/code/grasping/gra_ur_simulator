# LyonTech UR Simulator (docker)


## Set up and run simulator

- Once for all, build the docker image
    ```shell
    docker build -t lyontech_grasping/ur_simulator:v1 .  
    ```  

- Run container
    ```shell
    docker run -it --rm --name ur5e_simulator lyontech_grasping/ur_simulator:v1 .
    ```

- Copy / paste URL given by container output in a web browser
  
- Connect
  
- Start robot
  
- Enable remote control
    ![](img/enable_remote.png)

- Set external control as a program
    ![](img/external_control_program.png)
